
int getupLoopTotalTime = 5000;
int getupGetupTime = 3000;
int getupStayTime = 1950;

int getUpUpdate() {
  int lp = millis() % getupLoopTotalTime;
  float s = 0;
  if (lp < getupGetupTime) {
    s = map(lp, 0, getupGetupTime, 0, 180);
  } else if (lp < getupGetupTime + getupStayTime) {
    s = 180;
  } else {
    s = map(lp, getupGetupTime + getupStayTime, getupLoopTotalTime, 180, 0);
  }
  return (int)s;
}

