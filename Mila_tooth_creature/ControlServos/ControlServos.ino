// https://learn.adafruit.com/adafruit-arduino-lesson-14-servo-motors/if-the-servo-misbehaves

#include "SerialDebugging.h"
#include "Potmeters.h"
#include "ServoController.h"
#include "Circulator.h"
#include "RythmPlayer.h"
#include "Getup.h"
#include "NewDance.h"



// connect a switch between ground and 10.
// internal pull-up resistor will make sure we don't need an extra pull-up
#define SWITCH0_SIDE0 10
#define SWITCH0_SIDE1 11
// connect a toggle button to pin 12
// internal pull-up resistor will make sure we don't need an extra pull-up
#define MODE_SWITCH 12




int lastModeSwitchState = HIGH;
boolean directMode = true;



#define MODE_NEW_DANCE 0
#define MODE_FLAT0 1
#define MODE_RYTHM_PLAYER 2
#define MODE_FLAT1 3
#define MODE_CIRCULATOR 4
#define MODE_FLAT2 5

// these will now be ignored:
#define MODE_RANDOM 6
#define MODE_GET_UP 7
#define MODE_BOUNCE 8

#define MODE_COUNT 6

int mode = MODE_NEW_DANCE;

RythmPlayer rythmPlayer;
Circulator circulator;

float bounceDeg = 0;
float bounceDegAdd = 1;


void setup() {

  setupNewDance();
  attachServos();

  initSerialDebugging();

  s0 = s1 = s2 = s3 = 0;
  //  TIMSK0=0; // disable millis(), delay etc

  pinMode(SWITCH0_SIDE0, INPUT);
  pinMode(SWITCH0_SIDE1, INPUT);
  pinMode(MODE_SWITCH, INPUT);
  digitalWrite(SWITCH0_SIDE0, HIGH); // enable internal pull-up resistor
  digitalWrite(SWITCH0_SIDE1, HIGH); // enable internal pull-up resistor
  digitalWrite(MODE_SWITCH, HIGH);  // enable internal pull-up resistor
}

void loop() {

  // SWITCH & BUTTON TO CONTROL MODES:

  directMode = digitalRead(SWITCH0_SIDE0) == HIGH; // direct mode on switch0 HIGH
  int modeSwitchState = digitalRead(MODE_SWITCH);
  // on mode button down:
  if (modeSwitchState == LOW && lastModeSwitchState == HIGH) {
    //TODO: mode switch
    mode++;
    mode %= MODE_COUNT;
    initNewDance();
  }
  lastModeSwitchState = modeSwitchState;


  // READ POTENTIOMETERS

  readPotmeters();
  serialDebugValues(v0, v1, v2, v3);


  // UPDATE SERVO's

  // DIRECT MODE: control servo's from potentiometers

  if (directMode) {

    // adaptation for hand-up mode
    setServos(
      map(v0, 0, 1023, 0, 220),
      map(v1, 0, 1023, 0, 180),
      map(v2, 0, 1023, 0, 180),
      map(v3, 0, 1023, 0, 180));


  }



  if (!directMode) {

    switch (mode) {

      // RYTHM PLAYER

      case MODE_RYTHM_PLAYER:
        rythmPlayer.update();
        if (rythmPlayer.active) {
          rythmPlayer.ritmoDelay = (int)map(v0, 0, 1023, 100, 200);
          setServos(rythmPlayer.s0, rythmPlayer.s1, rythmPlayer.s2, rythmPlayer.s3);
        }
        break;

      case MODE_NEW_DANCE:
        updateNewDance();
        break;

      // RANDOM:

      case MODE_RANDOM:
        setServos((int)random(0, 180), (int)random(0, 180), (int)random(0, 180), (int)random(0, 180));
        break;

      case MODE_FLAT0:
      case MODE_FLAT1:
      case MODE_FLAT2:
        setServos(180, 180, 180, 180);
        break;

      // CIRCULATOR:

      case MODE_CIRCULATOR:
        circulator.update(millis());
        setServos(circulator.s0, circulator.s1, circulator.s2, circulator.s3);
        break;

      // GET UP:

      case MODE_GET_UP:
        int s = getUpUpdate();
        setServos(s, s, s, s);
        break;

    }

  }
  serialDebugValues(s0, s1, s2, s3);



  // DELAY

  if (directMode) {
    delay(15);
  } else {
    switch (mode) {
      // rythm player
      case MODE_RYTHM_PLAYER:
        delay(rythmPlayer.ritmoDelay);
        break;
      case MODE_RANDOM:
        delay(random(80, 1500));
        break;
      case MODE_CIRCULATOR:
        delay(circulator.delay);
        break;
      case MODE_NEW_DANCE:
        delay(5);
        break;
      case MODE_GET_UP:
        delay(15);
        break;
      case MODE_BOUNCE:
        delay(15);
        break;
    }
  }


}

