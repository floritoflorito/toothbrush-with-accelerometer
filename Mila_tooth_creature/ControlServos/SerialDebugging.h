#define SERIAL_DEBUG false

void initSerialDebugging() {
  if (SERIAL_DEBUG) {
    Serial.begin(115200);
  }
}

void serialDebugValues(int v0, int v1, int v2, int v3) {
  if (SERIAL_DEBUG) {
    Serial.print(v0);
    Serial.print(" ");
    Serial.print(v1);
    Serial.print(" ");
    Serial.print(v2);
    Serial.print(" ");
    Serial.print(v3);
    Serial.print(" ");
  }
}

