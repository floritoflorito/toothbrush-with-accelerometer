#include "Arduino.h"
#include "RythmPlayer.h"

RythmPlayer::RythmPlayer() {
  active = true;
  ritmoMax = 64;
  ritmoIndex = 0;
  ritmoDelay = 200;

  // la cuccaracha
  ritmo0 = "X..X..X.X..X..X.X..X..X.X..X..X.X..X..X.X..X..X.X..X..X.X.......";
  ritmo1 = "X..X.XXXX..X.....X.XXXXXX....XXXX..X.XXXX..X.....X.XXXXXX....XXX";
  ritmo2 = "X.......X.......X.......X.......X.......X.......X.......X.......";
  ritmo3 = "X..X....X..X............X.......X..X....X..X............X....XXX";
  
  ritmo0Step = 30;
  ritmo1Step = 30;
  ritmo2Step = 30;
  ritmo3Step = 30;
  s0 = 0;
  s1 = 0;
  s2 = 0;
  s3 = 0;
}

void RythmPlayer::update() {
  if (active) {

    char ritmoChar = ritmo0.charAt(ritmoIndex);
    if (ritmoChar == 'X') {
      s0 += ritmo0Step;
      if (ritmo0Step > 0 && s0 > 180) {
        s0 = 180 - ritmo0Step;
        ritmo0Step = -ritmo0Step;
      }
      else if (ritmo0Step < 0 && s0 < 0) {
        s0 = -ritmo0Step;
        ritmo0Step = -ritmo0Step;
      }
    }
    ritmoChar = ritmo1.charAt(ritmoIndex);
    if (ritmoChar == 'X') {
      s1 += ritmo1Step;
      if (ritmo1Step > 0 && s1 > 180) {
        s1 = 180 - ritmo1Step;
        ritmo1Step = -ritmo1Step;
      }
      else if (ritmo1Step < 0 && s1 < 0) {
        s1 = -ritmo1Step;
        ritmo1Step = -ritmo1Step;
      }
    }
    ritmoChar = ritmo2.charAt(ritmoIndex);
    if (ritmoChar == 'X') {
      s2 += ritmo2Step;
      if (ritmo2Step > 0 && s2 > 180) {
        s2 = 180 - ritmo2Step;
        ritmo2Step = -ritmo2Step;
      }
      else if (ritmo2Step < 0 && s2 < 0) {
        s2 = -ritmo2Step;
        ritmo2Step = -ritmo2Step;
      }
    }
    ritmoChar = ritmo3.charAt(ritmoIndex);
    if (ritmoChar == 'X') {
      s3 += ritmo3Step;
      if (ritmo3Step > 0 && s3 > 180) {
        s3 = 180 - ritmo3Step;
        ritmo3Step = -ritmo3Step;
      }
      else if (ritmo3Step < 0 && s3 < 0) {
        s3 = -ritmo3Step;
        ritmo3Step = -ritmo3Step;
      }
    }


    ritmoIndex++;
    ritmoIndex %= ritmoMax;
  }
}

