#include "Arduino.h"
#include "Circulator.h"

Circulator::Circulator() {
  delay = 15;
  s0=0;
  s1=0;
  s2=0;
  s3=0;
}

void Circulator::update(int millis) {
  radians = mapf(millis,0,3000,0,TWO_PI);
  s0 = (int)mapf(sin(radians+0*HALF_PI),-1,1,0,180);
  s1 = (int)mapf(sin(radians+1*HALF_PI),-1,1,0,180);
  s2 = (int)mapf(sin(radians+2*HALF_PI),-1,1,0,180);
  s3 = (int)mapf(sin(radians+3*HALF_PI),-1,1,0,180);
}

float Circulator::mapf(float x, float in_min, float in_max, float out_min, float out_max)
{
  return (x - in_min) * (out_max - out_min) / (in_max - in_min) + out_min;
}

