#ifndef Circulator_h
#define Circulator_h

#include "Arduino.h"
#include "Circulator.h"

class Circulator
{
  public:
    Circulator();
    void update(int millis);
    int delay;
    int s0, s1, s2, s3;
    float mapf(float x, float in_min, float in_max, float out_min, float out_max);
  private:
    float radians;
    
};

#endif

