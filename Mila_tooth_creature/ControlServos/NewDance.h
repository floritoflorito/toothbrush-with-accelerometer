
float ndTime = 0; // time in seconds
long ndStartTime = 0;

// 10 points = (6-1)*2
#define ND_POINTS 10

// 6 points
// A.A..A..A.A...
// ........B.B..B..B.B
// ................A.A
// 0.2..5..8.0..3..6.7
// 0.0..0..0.1..1..1.1
// ................X

float ndTimePoints[]  = {0, 2,   5,   8,  10, 20};//make last time longer than loop
float ndAnglePoints[] = {0, 180, 120, 180, 0,  0};
float ndLoopPointUnscaled = 16;  //unscaled

float ndTransitionTime = 0.3f; // to make transition (before stretch) <- make sure it' always smaller than steps inbetween ndTimePoints
float ndTimeFactor = 0.15f;    // to stretch time points

// 10 points = (6-1)*2
float ndTimes[ND_POINTS];
float ndAngles[ND_POINTS];
float ndLoopLength;

boolean ndSwap = false;

//TODO: foot 1&2  0->180->160->180->0
// the foot 3&4                  0->180->160->1800>-

void setupNewDance() {

  Serial.begin(9600);
  // 5 = 6-1
  for (int i = 0; i < 5; i++) {

    int ti0 = i * 2;
    int ti1 = i * 2 + 1;

    ndTimes[ti0] = ndTimeFactor * ndTimePoints[i];
    ndTimes[ti1] = ndTimeFactor * (ndTimePoints[i + 1] - ndTransitionTime);

    ndAngles[ti0] = 180 - ndAnglePoints[i];
    ndAngles[ti1] = 180 - ndAnglePoints[i];

  }

  ndLoopLength = ndTimeFactor * ndLoopPointUnscaled;

  for (int i = 0; i < ND_POINTS; i++) {
    Serial.print(ndTimes[i]);
    Serial.print(" ");
  }
  Serial.println();

  for (int i = 0; i < ND_POINTS; i++) {
    Serial.print(ndAngles[i]);
    Serial.print(" ");
  }
  Serial.println();
  Serial.println(ndLoopLength);
}



void initNewDance() {

  ndTime = 0;
  ndStartTime = millis();

}


void findAndInterpolate(float ti, boolean pair0) {

  // find correct points & interpolate
  int i0 = -1, i1 = -1;
  float time0, time1;
  for (int i = 0; i < ND_POINTS - 1; i++) {
    if (ti >= ndTimes[i] && ti < ndTimes[i + 1]) {
      time0 = ndTimes[i];
      time1 = ndTimes[i + 1];
      i0 = i;
      i1 = i + 1;
      break;
    }
  }

  // now interpolate
  if (i0 != -1) {
    float angle0 = ndAngles[i0];
    float angle1 = ndAngles[i1];

    int angle = (int)mapff(ti, time0, time1, angle0, angle1);

    if (pair0) setServos01(angle, angle);
    else setServos23(angle, angle);
  }

}



void updateNewDance() {

  ndTime = (millis() - ndStartTime) / 1000.0f;

  // loop:
  if (ndTime >= ndLoopLength) {
    ndTime -= ndLoopLength;
    ndStartTime += (ndLoopLength * 1000);
    //ndSwap = !ndSwap;
  }

  float ti = ndTime;
  findAndInterpolate(ti, true);
  
  ti = (ndTime + ndLoopLength / 2);
  if (ti >= ndLoopLength) ti -= ndLoopLength;
  findAndInterpolate(ti, false);

}




