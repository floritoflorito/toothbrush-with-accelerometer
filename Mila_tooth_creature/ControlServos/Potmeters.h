
// DEFINE THE ANALOG PINS THAT ARE CONNECTED TO THE POTENTIOMETERS

#define pot0 0
#define pot1 1
#define pot2 2
#define pot3 3

int v0, v1, v2, v3;

void readPotmeters() {
  v0 = analogRead(pot0);
  v1 = analogRead(pot1);
  v2 = analogRead(pot2);
  v3 = analogRead(pot3);
}

