#include <Servo.h>

#define servo0 9
#define servo1 6
#define servo2 5
#define servo3 3

#define MIN_SERVO 0.0f
#define MAX_SERVO 90.0f

Servo myservo0;  // create servo object to control a servo
Servo myservo1;  // create servo object to control a servo
Servo myservo2;  // create servo object to control a servo
Servo myservo3;  // create servo object to control a servo

int s0, s1, s2, s3; // servo value to set

void attachServos() {
  myservo0.attach(servo0);
  myservo1.attach(servo1);
  myservo2.attach(servo2);
  myservo3.attach(servo3);
}

float mapff(float x, float in_min, float in_max, float out_min, float out_max)
{
  return (x - in_min) * (out_max - out_min) / (in_max - in_min) + out_min;
}

void setServos(int d0, int d1, int d2, int d3) {
  s0 = d0;
  s1 = d1;
  s2 = d2;
  s3 = d3;
  myservo0.write( (int)mapff(d0,0,180,75,165) ); //d0 );//TODO: this one needs other range for free demo ->165=flat.
  myservo1.write( (int)mapff(d1,0,180,MIN_SERVO,MAX_SERVO) );
  myservo2.write( (int)mapff(d2,0,180,MIN_SERVO,MAX_SERVO) );
  myservo3.write( (int)mapff(d3,0,180,MIN_SERVO,MAX_SERVO) );
}

void setServos01(int d0, int d1) {
  s0 = d0;
  s1 = d1;
  myservo0.write( (int)mapff(d0,0,180,75,165) ); //d0 );//TODO: this one needs other range for free demo ->165=flat.
  myservo1.write( (int)mapff(d1,0,180,MIN_SERVO,MAX_SERVO) );
//  myservo2.write( (int)mapff(d2,0,180,MIN_SERVO,MAX_SERVO) );
//  myservo3.write( (int)mapff(d3,0,180,MIN_SERVO,MAX_SERVO) );
}

void setServos23(int d2, int d3) {
//  s0 = d0;
//  s1 = d1;
  s2 = d2;
  s3 = d3;
//  myservo0.write( (int)mapff(d0,0,180,75,165) ); //d0 );//TODO: this one needs other range for free demo ->165=flat.
//  myservo1.write( (int)mapff(d1,0,180,MIN_SERVO,MAX_SERVO) );
  myservo2.write( (int)mapff(d2,0,180,MIN_SERVO,MAX_SERVO) );
  myservo3.write( (int)mapff(d3,0,180,MIN_SERVO,MAX_SERVO) );
}





