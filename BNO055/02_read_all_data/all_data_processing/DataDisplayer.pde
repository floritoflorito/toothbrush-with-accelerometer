class DataDisplayer {

  float PIXELS_PER_POINT = 5;
  
  String label;
  float showWidth, showHeight;
  float valueLow, valueHigh;
  float x, y;
  int[] dataColors = new int[] {
    0xffff2222, 
    0xff66ff66, 
    0xffaaaaff
  };
  
  

  public DataDisplayer(String label, float showWidth, float showHeight, float valueLow, float valueHigh) {
    this.label=label;
    this.showWidth=showWidth;
    this.showHeight=showHeight;
    this.valueLow=valueLow;
    this.valueHigh=valueHigh;
  }

  void setPos(float x, float y) {
    this.x=x;
    this.y=y;
  }

  void drawComplete(ArrayList<PVector> dataPoints) {
    drawFrame();
    drawZeroLine();
    drawLabel();
    drawData(dataPoints);
  }

  void drawLabel() {
    textAlign(LEFT, BASELINE);
    fill(255);
    text(label, x+5, y+10+5);
  }

  void drawFrame() {
    noFill();
    stroke(255, 128);
    strokeWeight(1);
    rectMode(CORNER);
    rect(x, y, showWidth, showHeight);
  }

  void drawZeroLine() {
    float ly = y0+map(0, valueHigh, valueLow, 0, showHeight);
    stroke(255, 128);
    float w = 20;
    for (float lx=x; lx<=x+showWidth; lx+=w*2) {
      line(lx, ly, lx+w, ly);
    }
  }

  void drawData(ArrayList<PVector> dataPoints) {

    //stroke(255,128,128);
    //strokeWeight(2);

    ArrayList<PVector> dataXPoints = new ArrayList<PVector>();
    ArrayList<PVector> dataYPoints = new ArrayList<PVector>();
    ArrayList<PVector> dataZPoints = new ArrayList<PVector>();

    int maxPoints = (int)((x1-x0)/(float)PIXELS_PER_POINT);
    int i0 = dataPoints.size()-maxPoints;
    i0 = max(0,i0);
    for (int i=i0; i<dataPoints.size(); i++) {
      //for (PVector dataPoint:points) {

      PVector dataPoint=dataPoints.get(i);
      float drawPositionHorizontal = x+(i-i0)*PIXELS_PER_POINT;
      //PVector data = pit.data;
      //scale data to vertical size
      float drawPositionVertical_dataX = y + map(dataPoint.x, valueLow, valueHigh, showHeight, 0);
      float drawPositionVertical_dataY = y + map(dataPoint.y, valueLow, valueHigh, showHeight, 0);
      float drawPositionVertical_dataZ = y + map(dataPoint.z, valueLow, valueHigh, showHeight, 0);
      dataXPoints.add(new PVector(drawPositionHorizontal, drawPositionVertical_dataX));
      dataYPoints.add(new PVector(drawPositionHorizontal, drawPositionVertical_dataY));
      dataZPoints.add(new PVector(drawPositionHorizontal, drawPositionVertical_dataZ));
    }

    PVector[][] dataXYZPoints = new PVector[][] {
      dataXPoints.toArray(new PVector[0]), 
      dataYPoints.toArray(new PVector[0]), 
      dataZPoints.toArray(new PVector[0])
    };

    strokeWeight(1);
    for (int i=0; i<3; i++) {
      beginShape(LINE_STRIP);
      stroke(dataColors[i]);
      PVector[] points = dataXYZPoints[i];
      for (PVector p : points)
        vertex(p.x, p.y);
      endShape();
    }
  }
}
