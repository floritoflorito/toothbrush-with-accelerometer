boolean parseCalibrationData(String[] splits) {
  if (splits[0].equals("calibration")) {
    for (int i=0; i<splits.length; i++) {
      if (splits[i].equals("system") && splits.length>i+1) {
        calSys=parseInt(splits[i+1]);
      } else if (splits[i].equals("gyro") && splits.length>i+1) {
        calGyro=parseInt(splits[i+1]);
      } else if (splits[i].equals("accel") && splits.length>i+1) {
        calAcc=parseInt(splits[i+1]);
      } else if (splits[i].equals("mag") && splits.length>i+1) {
        calMag=parseInt(splits[i+1]);
      }
    }
    return true;
  }
  return false;
}

//eventtype 1 vector_accelerometer x -0.01 y 0.06 z 9.81
PVector getEventData3d(String[] splits, String type)  {
  if (splits.length>=9 && splits[0].equals("eventtype") && splits[2].equals(type)) {
    return new PVector(
      parseFloat(splits[4]), parseFloat(splits[6]), parseFloat(splits[8])
    );
  }
  return null;
}
