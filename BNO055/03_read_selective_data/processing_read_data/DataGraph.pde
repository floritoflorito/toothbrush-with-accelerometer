class DataGraph {
  
  ArrayList<Float> values = new ArrayList<Float>();
  
  String name = "dataGraph";
  float minValue;
  float maxValue;
  
  int maxValues = 500;
  int graphWidth = 500;
  int graphHeight = 200;
  int graphColor = 0xff000000;
  
  int runningAverageColor = 0xff884411;
  RunningAverage runningAverage = null;
  
  DataGraph(String name, float min, float max) {
    this.name = name;
    this.minValue = min;
    this.maxValue = max;
  }
  
  void addValue(float value) {
    values.add(value);
    while (values.size()>graphWidth) {
      values.remove(0);
    }
    
    if (runningAverage!=null) {
      runningAverage.addValue(value);
    }
    
  }
  
  void enableRunningAverage(int count) {
    runningAverage = new RunningAverage(count);
  }
  
  void draw(float x, float y) {
    pushMatrix();
    translate(x,y);
    
    textAlign(LEFT,TOP);
    textFont(verdana);
    fill(0);
    text(name,15,0);
    
    textAlign(LEFT,CENTER);
    text(maxValue,graphWidth+15+5,15);
    text(minValue,graphWidth+15+5,graphHeight+15);
    
    translate(15,15);
    stroke(graphColor);
    strokeWeight(1);
    noFill();
    
    
    beginShape(LINE_STRIP);
    Float[] vals = values.toArray(new Float[0]);
    for (int index=0;index<vals.length;index++) {
      float px = map(index,0,maxValues,0,graphWidth);
      try {
      float py = map(max(minValue,min(maxValue,vals[index])),minValue,maxValue,graphHeight,0);
      vertex(px,py);
      } catch (NullPointerException e) {
        e.printStackTrace();
      }
    }
    endShape();
    
    if (runningAverage!=null) {
      stroke(runningAverageColor);
      float py = map(max(minValue,min(maxValue,runningAverage.getRunningAverage())),minValue,maxValue,graphHeight,0);
      line(0,py,graphWidth,py);
      fill(runningAverageColor);
      text(runningAverage.getRunningAverage(), graphWidth+15+5, py);
    }
    
    stroke(0);
    noFill();
    rect(0,0,graphWidth,graphHeight);
    
    
    popMatrix();
  }
  
}
