class RunningAverage {

  private ArrayList<Float> values = new ArrayList<Float>();
  private int count;
  private float runningAverage = 0;

  RunningAverage(int count) {
    this.count = count;
  }

  void addValue(float value) {
    values.add(value);
    while (values.size()>count) {
      values.remove(0);
    }

    if (values.size()>0) {
      float total = 0;
      for (int i=0; i<values.size(); i++) {
        total += values.get(i);
      }
      total /= values.size();
      runningAverage = total;
    }
  }
  
  float getRunningAverage() {
    return runningAverage;
  }
  
}
