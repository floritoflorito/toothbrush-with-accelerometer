class VectorDataGraph {

  private DataGraph dataX, dataY, dataZ;

  VectorDataGraph(String name, float min, float max) {
    dataX = new DataGraph(name,min,max);
    dataX.graphColor = 0xaaff0000;
    dataY = new DataGraph("",min,max);
    dataY.graphColor = 0xaa00ff00;
    dataZ = new DataGraph("",min,max);
    dataZ.graphColor = 0xaa0000ff;
  }
  
  void setGraphHeight(int h) {
    dataX.graphHeight = dataY.graphHeight = dataZ.graphHeight = h;
  }
  int getGraphHeight() {
    return dataX.graphHeight;
  }
  
  void addValue(PVector v) {
    addValue(v.x,v.y,v.z);
  }

  void addValue(float vx, float vy, float vz) {
    dataX.addValue(vx);
    dataY.addValue(vy);
    dataZ.addValue(vz);
  }
  
  void draw(float x, float y) {
    dataX.draw(x, y);
    dataY.draw(x, y);
    dataZ.draw(x, y);
  }
  
}
