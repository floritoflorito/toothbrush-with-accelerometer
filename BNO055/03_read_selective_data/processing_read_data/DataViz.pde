
VectorDataGraph graphAccel;
DataGraph graphAccelSqrt;
VectorDataGraph graphOrientationEuler;

PFont verdana;



void initDataViz() {
  
  verdana = loadFont("Verdana-12.vlw");

  graphAccel = new VectorDataGraph("Linear acceleration", -4, 4);
  graphAccel.setGraphHeight(100);
  graphAccelSqrt = new DataGraph("Sqrt lin acc", 0, 4);
  graphAccelSqrt.graphHeight = 300;

  graphAccelSqrt.enableRunningAverage(25);

  graphOrientationEuler = new VectorDataGraph("Orientation euler", -360, 360);
  graphOrientationEuler.setGraphHeight(200);
}





void showData() {
  
  background(240);

  beginCamera();
  camera();
  graphAccel.draw(10, 10);
  graphAccelSqrt.draw(10, graphAccel.getGraphHeight()+30);
  graphOrientationEuler.draw(550, 10);
  endCamera();


  beginCamera();
  translate(300, 0);
  camera(0, 0, 300, 0, 0, 0, 0, 1, 0);
  pushMatrix();
  //translate(550,graphAccel.getGraphHeight()+30);
  // Simple 3 point lighting for dramatic effect.
  // Slightly red light in upper right, slightly blue light in upper left, and white light from behind.
  //pointLight(255, 200, 200,  400, 400,  500);
  //pointLight(200, 200, 255, -400, 400,  500);
  //pointLight(255, 255, 255,    0,   0, -500);
  lights();
  
  translate(150,75);

  float c1 = cos(radians(roll));
  float s1 = sin(radians(roll));
  float c2 = cos(-radians(pitch)); // intrinsic rotation
  float s2 = sin(-radians(pitch));
  float c3 = cos(radians(yaw));
  float s3 = sin(radians(yaw));
  applyMatrix( c2*c3, s1*s3+c1*c3*s2, c3*s1*s2-c1*s3, 0, 
    -s2, c1*c2, c2*s1, 0, 
    c2*s3, c1*s2*s3-c3*s1, c1*c3+s1*s2*s3, 0, 
    0, 0, 0, 1);
  
  pushMatrix();
  

  //lights();
  stroke(0);
  fill(192);
  box(140, 20, 75);
  popMatrix();
  popMatrix();
  endCamera();
}
