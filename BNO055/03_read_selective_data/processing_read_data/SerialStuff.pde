
final String POST_VECTOR_LINEAR_ACCELEROMETER_NAME  = "vla";
final String POST_VECTOR_EULER_NAME = "veu";
String PORT_NAME = "/dev/cu.usbserial-A1065T7H";//"/dev/cu.usbmodem141101";
final String PORT_SEARCH = "cu.usb"; // i.e. cu.usbmodem, cu.usbserial



float roll  = 0.0F;
float pitch = 0.0F;
float yaw   = 0.0F;



void initSerial() {
  try {
    port = new Serial(this, PORT_NAME, 115200);
  } 
  catch (RuntimeException e) {
    println("Port not found..?: "+e.toString());
    println("Available ports:");
    String[] ports = Serial.list();
    String alternative="";
    for (int i=0; i<ports.length; i++) {
      println("  "+ports[i]);
      if (ports[i].indexOf(PORT_SEARCH)>=0) {
        alternative = ports[i];
        break;
      }
    }
    if (alternative.length()>0) {
      println("Choosing alternative port: "+alternative);
      PORT_NAME = alternative;
      try {
        port = new Serial(this, PORT_NAME, 115200);
        println("YESSS");
      } catch (RuntimeException e1) {
        println("Sorry, also not working: "+e1.toString());
        println("Quitting");
        exit();
      }
    } else {
      println("Sorry, no chance. Quitting!");
      exit();
    }
  }
  port.bufferUntil('\n');
}



void serialEvent(Serial p) 
{
  String incoming = p.readString().trim();
  if (printSerial) {
    println(incoming);
  }

  String[] splits = incoming.split(" ");

  if (splits.length>3) {
    String firstWord = splits[0];

    switch (firstWord) {
    case POST_VECTOR_LINEAR_ACCELEROMETER_NAME:
      PVector linearAccelerometer = new PVector(parseFloat(splits[1]), parseFloat(splits[2]), parseFloat(splits[3]));
      graphAccel.addValue(linearAccelerometer);
      graphAccelSqrt.addValue( linearAccelerometer.mag() );
      break;

    case POST_VECTOR_EULER_NAME:
      PVector euler = new PVector(parseFloat(splits[1]), parseFloat(splits[2]), parseFloat(splits[3]));
      graphOrientationEuler.addValue(euler);

      roll  = float(splits[3]); // Roll = Z
      pitch = float(splits[2]); // Pitch = Y 
      yaw   = float(splits[1]); // Yaw/Heading = X

      break;
    }
  }
}
