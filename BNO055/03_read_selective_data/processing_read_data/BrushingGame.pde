float perfectBrushTime = 2;
float goodBrushingThresholdValue = 1.0;
float moveBrushThresholdValue = 1.5;



class BrushingGame {


  int toothAmount = 10;
  Tooth[] teeth;
  Tooth currentTooth;
  int currentToothIndex = 0;
  float totalRotY = 0;

  BrushingGame() {
    setup();
  }

  void setup() {

    teeth = new Tooth[toothAmount];
    for (int i=0; i<toothAmount; i++) {
      teeth[i] = new Tooth( map(i, 0, toothAmount, 0, 360 ) );
    }
    currentTooth = teeth[0];
    currentToothIndex=0;
    currentTooth.active = true;
  }


  void draw() {
    background(0x675617);

    //lights();
    ambientLight(64, 64, 64);
    lightFalloff(1, 0, 0);
    lightSpecular(0, 0, 0);
    directionalLight(192, 192, 192, 0, 0, -1); // from front

    camera(0, -600, 800, 0, 0, 200, 0, 1, 0);


    float frameTime = 1.0/frameRate;

    float accelAverage = graphAccelSqrt.runningAverage.getRunningAverage();

    // brush current tooth.
    if (currentTooth!=null) {
      if (accelAverage < goodBrushingThresholdValue) {
        currentTooth.brush(frameTime);
        if (currentTooth.brushPercentage==1) {
          currentToothIndex++;
          currentTooth.active = false;
          currentTooth = null;
          if (currentToothIndex == teeth.length) {
            System.out.println("YOU WON!");
            exit();
          }
        }
      }
    }

    if (currentTooth==null && accelAverage > moveBrushThresholdValue) {
      currentTooth = teeth[currentToothIndex];
      currentTooth.active = true;
    }

    if (currentTooth!=null) {
      totalRotY = 0.9*totalRotY + 0.1*radians(currentTooth.deg);
    }


    pushMatrix();
    rotateY(-totalRotY-HALF_PI);
    for (Tooth t : teeth)
      t.draw();
    popMatrix();

    //println(accelAverage);
  }
}





/*
*
 *    TOOTH
 *
 */

class Tooth {



  float brushTime = 0;
  float brushPercentage = 0;

  float wd=100, h=100;
  float rayStart = 150;
  float rayEnd = 250;
  float radius = 500;
  float deg;

  boolean active = false;

  Tooth(float deg) {
    this.deg = deg;
  }


  void brush(float seconds) {
    brushTime += seconds;
    brushPercentage = map(brushTime, 0, perfectBrushTime, 0, 1);
    brushPercentage = max(0, min(1, brushPercentage));
  }

  void draw() {






    //180,130,60 -> 255, 255, 255

    float perc = brushPercentage * 0.8;
    //float perc = map(mouseX,0,width,0,0.8);
    //float r=240, g=240, b=240;

    float r = map(perc, 0, 1, 180, 240);
    float g = map(perc, 0, 1, 130, 240);
    float b = map(perc, 0, 1, 60, 240);

    float rootWD = wd/5;
    float rootH = h*1.3;
    //directionalLight(r, g, b, 0, 0, -1); // from front
    //directionalLight(r, g, b, -1, 0, 0);// from right
    //directionalLight(r, g, b, 1, 0, 0);// from left
    //directionalLight(r, g, b, 0, 1, 0);// from top




    pushMatrix();

    rotateY(radians(deg));
    translate(radius, 0);

    pushMatrix();


    // CIRCLE UNDERNEATH
    if (active) {
      noStroke();
      fill(255);
      pushMatrix();
      translate(0, h+rootH);
      rotateX(HALF_PI);
      ellipseMode(RADIUS);
      ellipse(0, 0, wd, wd);
      popMatrix();
    }

    noStroke();
    fill(r, g, b);

    rotateY(radians(frameCount/2.0));
    box(wd, h, wd);



    for (int x=-1; x<=1; x+=2) {
      for (int z=-1; z<=1; z+=2) {
        pushMatrix();
        translate(x*(-wd/2+rootWD/2), h/2+rootH/2, z*(-wd/2+rootWD/2));
        box(wd/5, rootH, wd/5);
        popMatrix();
      }
    }
    popMatrix();


    noFill();

    // RAYS
    if (active) {
      stroke(r, g, b, perc*255);
      int amount = (int)map(perc, 0, 1, 0, 100);
      for (int i=0; i<amount; i++) {
        PVector dir = PVector.random3D();
        PVector begin = PVector.mult(dir, rayStart);
        PVector end = PVector.mult(dir, rayEnd);
        line(begin.x, begin.y, begin.z, end.x, end.y, end.z);
      }
    }

    popMatrix();
  }
}
